﻿<%@ Page Language="C#" AutoEventWireup="true" %>

<%@ Import Namespace="Wbm.OAuthV2SDK.OAuths" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>获取用户认证地址 - 我不忙-微博OAuth2SDK演示V3.1.0324</title>
</head>
<body>
    <div>
        <%
            /*
             * 3、获取用户认证地址。(参考Login.aspx文件)
             */
            var oauth_name = Request.QueryString["oauth"];
            if (!string.IsNullOrEmpty(oauth_name))
            {
                var oauth = OAuthBase.CreateInstance(oauth_name);
                if (oauth != null)
                {
                    oauth.UpdateCache(DateTime.Now.AddHours(1)); //缓存当前协议
                    var oauth_url = oauth.GetAuthorize(); //获取用户认证地址
                    Response.Redirect(oauth_url);
                }
                else
                {
                    Response.Write("登录失败，找不到相对应的接口");
                }
            }
            else
            {
                Response.Write("登录失败，找不到相对应的接口");
            }
        %>
    </div>
</body>
</html>
