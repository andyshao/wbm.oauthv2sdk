﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Text;
using System.Web;
using Wbm.OAuthV2SDK.OAuths;

namespace Wbm.OAuthV2Demo.Controllers
{
    /// <summary>
    /// 多个平台OAuth协议
    /// </summary>
    public class MoreOAuth
    {
        /// <summary>
        /// 多个平台发送微博
        /// </summary>
        /// <param name="text">微博内容</param>
        /// <returns></returns>
        public static string SendStatus(string text)
        {
            var msg = new StringBuilder();
            foreach (var item in Wbm.OAuthV2SDK.OAuthConfig.GetConfigOAuths())
            {
                try
                {
                    //模拟数据库获取accessToken
                    var token = HttpContext.Current.Session[item.name];
                    var uid = HttpContext.Current.Session[item.name + "_uid"];
                    if (token != null && uid != null)
                    {
                        string accessToken = token.ToString(); //从数据库读取
                        OAuthBase oauth = OAuthBase.CreateInstance(item.name);
                        oauth.Uid = uid.ToString();//腾讯微博必需参数
                        var result = oauth.SendStatus(accessToken, text);
                        if (result.ret != 0)
                        {
                            msg.AppendFormat("{0}发生错误：{1} <br>", item.name, result.msg);
                        }
                    }
                }
                catch (Exception ex)
                {
                    msg.AppendFormat("{0}发生异常：{1} <br>", item.name, ex.Message);
                }
            }
            return msg.ToString();
        }
        /// <summary>
        /// 多个平台发送图片微博
        /// </summary>
        /// <param name="text">微博内容</param>
        /// <param name="filename">文件名</param>
        /// <returns></returns>
        public static string SendStatusWithPic(string text, string filename)
        {
            var msg = new StringBuilder();
            foreach (var item in Wbm.OAuthV2SDK.OAuthConfig.GetConfigOAuths())
            {
                try
                {
                    var token = HttpContext.Current.Session[item.name];
                    var uid = HttpContext.Current.Session[item.name + "_uid"];
                    if (token != null && uid != null)
                    {
                        string accessToken = token.ToString(); //从数据库读取
                        OAuthBase oauth = OAuthBase.CreateInstance(item.name);
                        oauth.Uid = uid.ToString();//腾讯微博必需参数
                        var result = oauth.SendStatusWithPic(accessToken, text, filename);
                        if (result.ret != 0)
                        {
                            msg.AppendFormat("{0}发生错误：{1} <br>", item.name, result.msg);
                        }
                    }
                }
                catch (Exception ex)
                {
                    msg.AppendFormat("{0}发生异常：{1} <br>", item.name, ex.Message);
                }
            }
            return msg.ToString();
        }
    }
}
/*
 * Author: xusion
 * Created: 2012.07.25
 * Support: http://wobumang.com
 */