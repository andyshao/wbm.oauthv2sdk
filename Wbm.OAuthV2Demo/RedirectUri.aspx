﻿<%@ Page Language="C#" AutoEventWireup="true" %>

<%@ Import Namespace="Wbm.OAuthV2SDK.OAuths" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>获取认证信息 - 我不忙-微博OAuth2SDK演示V3.1.0324</title>
</head>
<body>
    <div>
        <% 
            /*
             *  4、获取/缓存认证信息。(参考RedirectUri.aspx文件)
             */
            if (OAuthBase.HasCacheOAuth)
            {
                var oauth = OAuthBase.CreateInstance();
                if (oauth != null)
                {
                    try
                    {
                        var token = oauth.GetAccessToken(); //获取认证信息
                        if (token.ret == 0)
                        {
                            oauth.AccessToken = token.access_token;
                            oauth.ExpiresIn = token.expires_in;
                            oauth.UpdateCache(); //缓存认证信息

                            //获取用户id
                            var result = oauth.GetUid();
                            if (result.ret == 0)
                            {
                                var uid = result.data;

                                //模拟数据库保存accessToken
                                var oauth_name = oauth.OAuthName;
                                Session[oauth_name] = token.access_token;
                                Session[oauth_name + "_uid"] = uid;
                                Response.Redirect("./");
                            }
                            else
                            {
                                Response.Write(result.msg + "(" + result.errcode + ")");
                                Response.Write("<br />" + result.response);
                            }
                        }
                        else
                        {
                            Response.Write(token.msg + "(" + token.errcode + ")");
                            Response.Write("<br />" + token.response);
                        }

                    }
                    catch (Exception ex)
                    {
                        Response.Write(ex.Message);
                    }
                }
                else
                {
                    Response.Write("登录失败，找不到相对应的接口");
                }
            }
            else
            {
                Response.Write("登录失败，找不到相对应的缓存");
            }
                       
        %>
    </div>
</body>
</html>
