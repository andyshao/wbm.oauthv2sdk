﻿using System;
namespace Wbm.OAuthV2SDK.OAuths.Vdisks.Models
{
    /// <summary>
    /// 实体类MUsers 。
    /// </summary>
    [Serializable]
    public class VdiskMToken : VdiskMError
    {
        /// <summary>
        /// 访问令牌 
        /// </summary>
        public string access_token { set; get; }
        /// <summary>
        /// 过期时间
        /// </summary>
        public int expires_in { set; get; }
        /// <summary>
        /// 刷新令牌
        /// </summary>
        public string refresh_token { set; get; }
        /// <summary>
        /// 剩余时间
        /// </summary>
        public int time_left { set; get; }
        /// <summary>
        /// 用户id
        /// </summary>
        public string uid { set; get; }
        /// <summary>
        /// 错误码
        /// </summary>
        public int code { set; get; }
        /// <summary>
        /// 错误信息
        /// </summary>
        public string msg { set; get; }
    }
}
/*
 * Author: xusion
 * Created: 2012.04.10
 * Support: http://wobumang.com
 */