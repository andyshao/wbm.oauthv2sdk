﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Text;
using Wbm.OAuthV2SDK.OAuths;
using Wbm.OAuthV2SDK.Entitys;
using Wbm.OAuthV2SDK.Helpers;
using Wbm.OAuthV2SDK.OAuths.Vdisks.Models;

namespace Wbm.OAuthV2SDK.OAuths.Vdisks
{
    /// <summary>
    /// 新浪微盘协议
    /// </summary>
    public class VdiskOAuth : OAuthBase
    {
        /// <summary>
        /// 协议节点名称(区分大小写)
        /// </summary>
        public override string OAuthName { get { return "vdisk"; } }
        /// <summary>
        /// 协议节点描述
        /// </summary>
        public override string OAuthDesc { get { return "新浪微盘"; } }

        /// <summary>
        /// 用户UID
        /// </summary>
        private string uid = string.Empty;

        public VdiskOAuth() { }

        public VdiskOAuth(string cfgAppName) { }

        /// <summary>
        /// 获取授权过的Access Token
        /// </summary>
        public override ApiToken GetAccessToken()
        {
            string accessTokenUrl = OAuthConfig.GetConfigAPI(OAuthName, this.App.AppName, "access_token");
            string response = GetAccessToken(accessTokenUrl);
            VdiskMToken token = UtilHelper.ParseJson<VdiskMToken>(response);
            ApiToken api = new ApiToken();
            api.request = "access_token";
            api.response = response;
            if (token.code == 0)
            {
                api.access_token = token.access_token;
                api.refresh_token = token.refresh_token;
                api.expires_in = token.expires_in;
                this.uid = token.uid;
            }
            else
            {
                api.ret = 1;
                api.errcode = Convert.ToString(token.code);
                api.msg = token.msg;
            }
            return api;
        }

        /// <summary>
        /// 获取用户id
        /// <para>data: 当前的返回Uid</para>
        /// </summary>
        /// <param name="accessToken">访问令牌</param>
        /// <returns></returns>
        public override ApiResult GetUid(string accessToken)
        {
            ApiResult api = new ApiResult();
            api.data = this.uid;
            return api;
        }


        /// <summary>
        /// 发送微博
        ///  <para>data: 当前的返回status_id</para>
        /// </summary>
        /// <param name="accessToken">访问令牌</param>
        /// <param name="strText">微博内容</param>
        /// <returns></returns>
        public override ApiResult SendStatus(string accessToken, string strText)
        {
            //官方暂无接口
            ApiResult api = new ApiResult();
            api.ret = 1;
            api.errcode = "1";
            api.msg = "官方暂无接口";
            return api;
        }

        /// <summary>
        /// 发送图片微博
        /// </summary>
        /// <param name="accessToken">访问令牌</param>
        /// <param name="strText">微博内容</param>
        /// <param name="strFile">图片绝对路径</param>
        /// <returns></returns>
        public override ApiResult SendStatusWithPic(string accessToken, string strText, string strFile)
        {
            //官方暂无接口
            ApiResult api = new ApiResult();
            api.ret = 1;
            api.errcode = "1";
            api.msg = "官方暂无接口";
            return api;
        }
    }
}
/*
 * Author: xusion
 * Created: 2012.07.25
 * Support: http://wobumang.com
 */